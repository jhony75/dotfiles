let mapleader =";"

set nocompatible

"Plugins
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
  \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
  "Quality of life
  Plug 'scrooloose/nerdtree'
  Plug 'junegunn/goyo.vim'
  Plug 'junegunn/limelight.vim'
  Plug 'vimwiki/vimwiki'
  Plug 'bling/vim-airline'
  Plug 'neoclide/coc.nvim', {'branch':'release'}
  Plug 'alvan/vim-closetag' "Close tags, more configuration possible.
  Plug 'tpope/vim-surround' "Change the insede of a tag.
  Plug 'tpope/vim-commentary' "Create a commentary like VSCode Ctrl+/
  "Plug 'yggdroot/indentline'
  Plug 'vim-syntastic/syntastic' "Syntax checker
  Plug 'majutsushi/tagbar' "Adds a bar to manage tags in a code. Needs to be mapped
  Plug 'flazz/vim-colorschemes'
  "Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' } "Colors highlighting

  "Languages support

  "Ruby
  Plug 'tpope/vim-rails', {'for': 'ruby'}
  Plug 'vim-ruby/vim-ruby', {'for': 'ruby'}
  Plug 'jgdavey/vim-blockle' , {'for': 'ruby'} "Move between do and end on a ruby block
  Plug 'cespare/vim-toml', {'for': 'ruby'}


  "Rust
  Plug 'rust-lang/rust.vim' , {'for': 'rust'}

  "Antlr
  Plug 'dylon/vim-antlr', {'for': ['g', 'g4']}

  "Syntax highlighting
  Plug 'PotatoesMaster/i3-vim-syntax'
  Plug 'kovetskiy/sxhkd-vim'
  Plug 'VebbNix/lf-vim'

call plug#end()

"Basic config
syntax on
syntax enable
colorscheme monokai

set relativenumber "With the set number option, shows the current line real number.
set number
set autoindent
set laststatus=2 "Always show the status line.
set ignorecase
set smartcase
set encoding=utf-8
set termguicolors "Ensures truecolor is activated, needed for Hexokinase.
set clipboard+=unnamedplus "Makes the clipboard the same as the system one.

"Vertically center document when entering insert mode
autocmd InsertEnter * norm zz

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"Converts tabs to spaces. 2 spaces for each tab.
set tabstop=2 shiftwidth=2 expandtab

"Pane moving
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set cursorline
highlight CursorLine ctermbg=Yellow cterm=bold guibg=#403e36

"Skeleton files
if has("autocmd")
  augroup templates
    autocmd BufNewFile *.rb 0r ~/.vim/templates/skeleton.rb
    autocmd BufNewFile *.py 0r ~/.vim/templates/skeleton.py
    autocmd BufNewFile *.sh 0r ~/.vim/templates/skeleton.sh
    autocmd BufNewFile *.html 0r ~/.vim/templates/skeleton.html
    autocmd BufNewFile *.rmd 0r ~/.vim/templates/skeleton.rmd
    autocmd BufNewFile *.cpp 0r ~/.vim/templates/skeleton.cpp
    autocmd BufNewFile * %substitute#\[:VIM_EVAL:\]\(.\{-\}\)\[:END_EVAL:\]#\=eval(submatch(1))#ge "Better skeleton files
  augroup END
endif

"Sources configurations

source ~/.config/nvim/sources/coc.vim
"source ~/.config/nvim/sources/hexokinase.vim
source ~/.config/nvim/sources/commands.vim

"Syntax highlighting for specific files
autocmd BufNewFile,BufRead ~/.config/polybar/config set syntax=dosini

let type = fnamemodify(bufname("%"), ":t:e")

if type == "rmd"
 source ~/.config/nvim/sources/rmarkdown.vim
endif
