"Manages the Hexokinase options.
"Hexokinase is a plugin like Atom Pigments (shows colors used in the code)

" Vim Hexokinase
let g:Hexokinase_refreshEvents = ['InsertLeave']
  let g:Hexokinase_optInPatterns = [
 \     'full_hex',
 \     'triple_hex',
 \     'rgb',
 \     'rgba',
 \     'hsl',
 \     'hsla',
 \     'colour_names'
 \ ]

let g:Hexokinase_highlighters = ['virtual']

" Reenable hexokinase on enter
autocmd VimEnter * HexokinaseTurnOn
