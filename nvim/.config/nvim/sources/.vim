" Enable and disable auto comment
map <leader>m :setlocal formatoptions-=cro<CR>
map <leader>M :setlocal formatoptions=cro<CR>

" Enable spell checking, s for spell check
map <leader>s :setlocal spell! spelllang=pt_br<CR>

"Inserts the current date
nmap <F3> i<C-R>=strftime("%d/%m/%Y")<CR><Esc>

"Inserts the current date with time
nmap <leader><F3> i<C-R>=strftime("%d/%m/%Y - %H:%M")<CR><Esc>

"Compile things
map <F5> :w! \| !compiler <c-r>%<CR>

"Open corresponding .pdf/.html or preview
map <F9> :!opout <c-r>%<CR><CR>

"Activates or deactivates tabs and end of line character marks.
map <leader>L
  \ :set list<CR>
  \ :set listchars=tab:§\ ,eol:¬<CR>

map <leader>l
  \ :set nolist<CR>
