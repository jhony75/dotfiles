"Manages R Markdown configurations

"Activates Conceal, may break the indentLine plugin

let g:indentLine_concealcursor = 'inc'
let g:indentLine_conceallevel = 0


"Mappings
autocmd Filetype rmd inoremap <leader>c ç
autocmd Filetype rmd inoremap <leader>C Ç

"Functions and related commands
function! BibleTemplate()
  r ~/.config/nvim/sources/templateBiblia.txt
endfunction

autocmd Filetype rmd nmap <leader><F2> :call BibleTemplate()<CR>

"Automatic activation of Goyo and Limelight

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

"Map to move in real lines, not logical ones

map j gj
map k gk
